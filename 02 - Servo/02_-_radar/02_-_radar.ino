#include <Servo.h>
Servo _servo;
int _deg = 0;
bool _inc = true;


// red +3v
// brown gnd
// orange 9

void setup() {
  _servo.attach(9);
}


void loop() {
  if(_inc){
    _deg += 1;
  }else{
    _deg -= 1;
  }

  if(_deg == 180){ _inc = false;}
  if(_deg == 0){ _inc = true;}
  
  _servo.write(_deg);
  delay(10);
}
