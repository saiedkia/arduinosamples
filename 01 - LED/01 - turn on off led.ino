void setup() {
  pinMode(9, OUTPUT);
}

void loop() {
  digitalWrite(9, HIGH); // 5V
  delay(1000);          
  digitalWrite(9, LOW); // 0V
  delay(1000);         
}
