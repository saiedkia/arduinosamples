int led = 9;          
int input = A0;

void setup() {
  pinMode(led, OUTPUT);
  pinMode(input, INPUT);
}

void loop() {
  int val = analogRead(input);
  Serial.print(val);
  analogWrite(led, val);
  
  //delay(30);
}
