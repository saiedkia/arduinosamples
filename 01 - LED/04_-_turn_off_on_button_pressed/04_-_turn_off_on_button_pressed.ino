int led = 9;          
int input = 10;

void setup() {
  pinMode(led, OUTPUT);
  pinMode(input, INPUT);
}

void loop() {
  int pressed = digitalRead(input);
  if (pressed){
    digitalWrite(led, HIGH);  
  }else{
    digitalWrite(led, LOW);
  }
  
  //delay(30);
}
