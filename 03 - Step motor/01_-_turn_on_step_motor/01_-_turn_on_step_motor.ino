#include <Stepper.h>
#define STEPS 6
Stepper _stepper(STEPS, 8, 9, 10, 11);

void setup() {
  // put your setup code here, to run once:
  _stepper.setSpeed(600);
}

void loop() {
  // put your main code here, to run repeatedly:
  _stepper.step(100);
}
